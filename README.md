# simple-components

The idea of the repository is to create simple components that can be reused simply and with even simpler configuration.

From that mobile menu, to a complete form, through loaders, cards, tooltips, and other endless possibilities.

I will start with my main use stack, which is `vue` + `pug` + `stylus`, but I accept translation contributions to other interesting stacks for greater coverage.

Soon I will deliver home with the partial preview and snapshot of the components, as well as tests of them in a functional way.

I'll be using codepen for snippets a lot, and testing, and gitlab for source repository, wait!

